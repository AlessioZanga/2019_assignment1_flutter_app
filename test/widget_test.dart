import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter/material.dart';

void main() {

  testWidgets('AppBar has a title', (WidgetTester tester) async {
    await tester.pumpWidget(new MyApp());
    final buttons = find.text('Vota i tuoi Cantanti preferiti');
    expect(buttons, findsOneWidget);
  });

  testWidgets('Tap', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    await tester.tap(find.byType(Padding));
  });

  testWidgets('finds a specific widget instance', (WidgetTester tester) async {
    final childWidget = Padding(padding: EdgeInsets.only(top: 20.0));

    // Provide the childWidget to the Container.
    await tester.pumpWidget(childWidget);

    // Search for the childWidget in the tree and verify it exists.
    expect(find.byWidget(childWidget), findsOneWidget);
  });

}

