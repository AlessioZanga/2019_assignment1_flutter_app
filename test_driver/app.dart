import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/main.dart';

void main() {
  enableFlutterDriverExtension();
  
  runApp(MyApp());
}