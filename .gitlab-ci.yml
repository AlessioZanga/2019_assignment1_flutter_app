# Docker image with Flutter installed
image: cirrusci/flutter:latest

# Global environment  variables
variables:
  ANDROID_EMULATOR: "system-images;android-25;google_apis;arm64-v8a"

# Defining stages of our CD/CI pipeline
stages:
  - build
  - verify
  - unit-test
  - integration-test
  - package
  - release
  - deploy

# Global cache path
cache:
  paths:
    - .pub-cache
    - android/gradlew
    - android/local.properties
    - android/gradle/wrapper/gradle-wrapper.jar

# Before all jobs load the cache 
# and create the app keystore for distribution on Google Play Store
default:
  before_script:
    - export PUB_CACHE=$CI_PROJECT_DIR/.pub-cache
    - echo "$UPLOAD_KEYSTORE" | base64 --decode > $CI_PROJECT_DIR/android/app/key.jks

# Build and compile app apk
compile_android_apk:
  stage: build
  script:
    - flutter build apk
  artifacts:
    paths:
      - build/app/outputs/apk

# Build and compile a release app bundle
compile_android_appbundle:
  stage: build
  script:
    - flutter build appbundle
  artifacts:
    paths:
      - build/app/outputs/bundle

# Analyze source code to find programming and stylistic errors
lint:
  stage: verify
  script:
    - flutter analyze
  allow_failure: true

# Unit test of single functions
unit_test:
  stage: unit-test
  script:
    - flutter test

# Integration test of multiple classes
integration_test:
  stage: integration-test
  script:
    - adb server
    - sdkmanager $ANDROID_EMULATOR
    - echo no | avdmanager create avd -n flutter_emulator -k $ANDROID_EMULATOR
    - $ANDROID_SDK_ROOT/emulator/emulator-headless -avd flutter_emulator -gpu swiftshader_indirect -no-window -no-audio -no-snapshot &
    - sleep 10
    - flutter drive --target=test_driver/app.dart
  # Temporary allow failure while looking for proper Android Emulator config in Docker
  allow_failure: true

# Package dependencies, libraries and apk of our app
package:
  stage: package
  script: 
    - mkdir package
    - cp -r build/app/outputs/apk package
  artifacts:
    paths: 
      - package

# Create a release of our app in the releases page on Gitlab
release:
  stage: release
  script:
    # Initialize Gradle and dependencies
    - ./android/gradlew -p android/app -q --console=plain printVersionName
    # Execute last task without initialization logs
    - export APP_VERSION=v`./android/gradlew -p android/app -q --console=plain printVersionName`
    - curl --header 'Content-Type:application/json' --header "PRIVATE-TOKEN:$PRIVATE_TOKEN" --data '{"name":"Assignment 1 - Release '"$APP_VERSION"'", "tag_name":"'"$APP_VERSION"'", "ref":"'"$CI_COMMIT_SHA"'", "description":"Release '"$APP_VERSION"'"}' --request POST "https://gitlab.com/api/v4/projects/14725881/releases" --verbose
  
  # Execute only on master branch
  only:
    - master

# Deploy the app on Google Play Store using Fastlane
deploy:
  stage: deploy
  script:
    - echo "$FASTLANE_KEY" | base64 --decode > $CI_PROJECT_DIR/android/fastlane-key.json
    - sudo gem install fastlane
    - cd android
    - fastlane deploy
  
  # Execute only on master branch
  only:
    - master

