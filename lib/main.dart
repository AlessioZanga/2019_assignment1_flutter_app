import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cantanti',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  final List <bool> isFavorited = [true,true,true,true,true];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vota i tuoi Cantanti preferiti')),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('cantanti').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: /*snapshot.map((data) => _buildListItem(context, data)).toList()*/
      [
        Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: ListTile(
              title: Text(snapshot[0]['name']),
              trailing: Text('${snapshot[0]['votes']}'),
              onTap: () =>

                  setState(() {
                    if (!isFavorited[0]) {
                      snapshot[0].reference.updateData({'votes': FieldValue.increment(-1)});
                      isFavorited[0] = true;
                    }else {
                      snapshot[0].reference.updateData({'votes': FieldValue.increment(1)});
                      isFavorited[0] = false;
                    }
                  })
          ),
        ),
      ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: ListTile(
                title: Text(snapshot[1]['name']),
                trailing: Text('${snapshot[1]['votes']}'),
                onTap: () =>

                    setState(() {
                      if (!isFavorited[1]) {
                        snapshot[1].reference.updateData({'votes': FieldValue.increment(-1)});
                        isFavorited[1] = true;
                      }else {
                        snapshot[1].reference.updateData({'votes': FieldValue.increment(1)});
                        isFavorited[1] = false;
                      }
                    })
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: ListTile(
                title: Text(snapshot[2]['name']),
                trailing: Text('${snapshot[2]['votes']}'),
                onTap: () =>

                    setState(() {
                      if (!isFavorited[2]) {
                        snapshot[2].reference.updateData({'votes': FieldValue.increment(-1)});
                        isFavorited[2] = true;
                      }else {
                        snapshot[2].reference.updateData({'votes': FieldValue.increment(1)});
                        isFavorited[2] = false;
                      }
                    })
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: ListTile(
                title: Text(snapshot[3]['name']),
                trailing: Text('${snapshot[3]['votes']}'),
                onTap: () =>

                    setState(() {
                      if (!isFavorited[3]) {
                        snapshot[3].reference.updateData({'votes': FieldValue.increment(-1)});
                        isFavorited[3] = true;
                      }else {
                        snapshot[3].reference.updateData({'votes': FieldValue.increment(1)});
                        isFavorited[3] = false;
                      }
                    })
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: ListTile(
                title: Text(snapshot[4]['name']),
                trailing: Text('${snapshot[4]['votes']}'),
                onTap: () =>

                    setState(() {
                      if (!isFavorited[4]) {
                        snapshot[4].reference.updateData({'votes': FieldValue.increment(-1)});
                        isFavorited[4] = true;
                      }else {
                        snapshot[4].reference.updateData({'votes': FieldValue.increment(1)});
                        isFavorited[4] = false;
                      }
                    })
            ),
          ),
        ),
      ]
    );
  }
}

class Record {
  final String name;
  final int votes;
  final DocumentReference reference;

  Record.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['name'] != null),
        assert(map['votes'] != null),
        name = map['name'],
        votes = map['votes'];

  Record.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  @override
  String toString() => "Record<$name:$votes>";
}
