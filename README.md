# Corso di Processo e Sviluppo del Software - Assignment 1 - Flutter App

## Indirizzo della Repository

<https://gitlab.com/AlessioZanga/2019_assignment1_flutter_app>

## Indirizzo Play Store 

<https://play.google.com/store/apps/details?id=com.alessiozanga.flutter_app&gl=IT>

## Elenco membri del gruppo

- 816385    Cavenaghi Emanuele
- 813473    Simone Ritucci
- 816462    Turano Andrea
- 815997    Zanga Alessio


## Breve descrizione del programma

Il programma scelto è una semplice applicazione realizzata in 
[Flutter](https://flutter.dev/) per android. L'applicazione permette di 
esprimere la propria preferenza per uno o piu dei cinque cantanti presenti, per
ogni cantante è esprimibile una sola preferenza e un secondo click eliminerà la
preferenza espressa precedentemente per il cantante stesso.

Le preferenze espresse vengono salvate su un database esterno ospitato sul 
servizio [Firebase](https://firebase.google.com/) e vengono aggiornate in 
realtime su ogni dispositivo.



## Scelte implementative della pipeline
Per poter costruire l'applicazione e gli stage seguenti della pipeline viene caricata 
all'inizio di questa l'immagine di Flutter recuperandola dal link 
[cirrusci/flutter:latest](https://hub.docker.com/r/cirrusci/flutter/ "cirrusci/flutter:latest").
Per velocizzare il lavoro della pipeline vengono salvati come cache le dipendenze 
di Flutter e il compiler di Android.


## Stage implementati 

Gli stage implementati sono sette e per ognuno vengono di seguito specificate 
le scelte implementative prese.

#### - Built
Nella fase di built si è scelto di creare sia un file in formato apk che bundle. 
La versione APK è utile per il package poichè universale; il bundle per il 
PlayStore perché permette di ottimizzare le dimensioni a seconda che 
il dispositivo usi arm o arm64.

#### - Verify
In questa fase viene svolta un'analisi statica della qualità del codice utilizzando 
lo strumento presente per il linguaggio dart. Gli errori qui riscontrati vengono 
solo segnalati e non viene bloccata la pipeline in quanto, benchè si ritenga utile 
produrre codice di buona qualità, la correzione di questi errori può avvenire in un 
secondo momento non inficiando sul funzionamento del programma.

#### - Unit Test
Vengono svolti gli unit test per verificare il funzionamento dei componenti 
di base dell'applicazione.

#### - Integration Test
Gli integration test testano la connessione dell'applicazione al database di Firestone. 
Viene quindi creato un emulatore android in Docker e vengono quindi svolti 
gli integration test presenti. Vengono ammessi fallimenti dato che l'obbiettivo
dell'assignment non è quello di emulare un terminale Android in Docker (come 
specificato dal prof. Mariani).

#### - Package
Nella fase di package viene semplicemente creato un package dei file necessari 
al funzionamento dell'applicazione. In particoare, nello sviluppo di un'applicazione
Android il prodotto della fase di package corrisponde al solo APK prodotto nella 
fase di built.

#### - Release
Eseguita solo per il branch master, viene creata una release sfruttando 
le realease api di GitLab.

#### - Deploy
Nella fase di deploy viene caricata l'applicazione tramite 
[fastlane](https://docs.fastlane.tools/ "fastlane") ma questo avviene solo se il branch è 
quello **master** cioè dove le versioni sono sufficientemente mature per essere aperte al pubblico.
